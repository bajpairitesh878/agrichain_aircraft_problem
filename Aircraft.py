import time

row, column = 3, 8
avail_seat = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3']
]
test_case = [4, 3, 3, 2, 2, 4, 1, 2]
avail_cseat = avail_seat.copy()
occupied_seat = []


def remove_func(ls, value):
    for i in value:
        ls.remove(i)


def assign_seat(n):
    # assiging middle seat
    for i in range(len(avail_cseat)):
        mid = len(avail_cseat[i])//n
        if len(avail_cseat[i]) > n+mid and len(avail_cseat[i]) >= column:
            seat = avail_cseat[i][mid:n+mid]
            occupied_seat.append(seat)
            remove_func(avail_cseat[i], seat)
            return ", ".join(seat)

    for i in range(len(avail_cseat)):
        mid = len(avail_cseat[i])//n
        if len(avail_cseat[i]) >= n+mid:
            seat = avail_cseat[i][:n]
            occupied_seat.append(seat)
            remove_func(avail_cseat[i], seat)
            return ", ".join(seat)

    for i in range(len(avail_cseat)):
        seat = avail_cseat[i][:n]
        occupied_seat.append(seat)
        remove_func(avail_cseat[i], seat)
        return ", ".join(seat)
    else:
        print('Opps! Seat are full.')


# test cases
if __name__ ==  "__main__":
    print(chr(27) + "[2J")
    start_time = time.time()
    for i in test_case:
        print(assign_seat(i))
    print(f'Execution time is: {time.time()-start_time}')
